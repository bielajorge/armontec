import { Component, OnInit } from '@angular/core';
import { Restaurant } from '../../restaurants/restaurant/restaurant.model';
import { RestaurantsService } from '../../restaurants/restaurants.service';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { MenuItem } from '../menu-item/menu-item.model';
import { Observable } from '../../../../node_modules/rxjs/Observable';

@Component({
  selector: 'mt-menu',
  templateUrl: './menu.component.html'
})

export class MenuComponent implements OnInit {

  menu: Observable<MenuItem[]>
  constructor(private restaurantsService: RestaurantsService,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.menu = this.restaurantsService.menuOfRestaurant(this.route.parent.snapshot.params['id'])
  }

  addMenuItem(item: MenuItem) {
   console.log(item)
  }
}
